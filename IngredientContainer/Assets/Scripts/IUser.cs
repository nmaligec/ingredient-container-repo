﻿using UnityEngine;

/// <summary>
/// Provides functionality for an object to decide how it is 
/// being affected by the object that is using it.
/// </summary>
public interface IUser
{
    /// <summary>
    /// Allows the controlling object's movement to affect the object it is using.
    /// </summary>
    void ControlMovement();

    /// <summary>
    /// No longer affect the movement of the object being used.
    /// </summary>
    void ReleaseMovement();

    /// <summary>
    /// Notify the controlling object that the interaction is no longer valid.
    /// </summary>
    /// <remarks>
    /// This will most likely happen when some other event 
    /// causes the object being used to respawn.
    /// </remarks>
    void CancelUsing();
}
