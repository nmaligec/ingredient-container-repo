﻿/// <summary>
/// Provides functionality for an object to be used by a controlling object.
/// </summary>
public interface IUseable
{
    /// <summary>
    /// Sets the object's state to that of being interacted with by another object.
    /// </summary>
    /// <param name="user">
    /// The object that is controlling this object during the interaction.
    /// </param>
    void StartUse(IUser user);

    /// <summary>
    /// Sets the object's state to no longer being interacted with by another object.
    /// </summary>
    void StopUse();
}
