﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages multiple <see cref="Shelf"/> objects and handles Crank mechanics.
/// </summary>
/// <remarks>
/// Shelves moves up or down as the Crank is interacted with.
/// New Shelf objects are added as needed.
/// When two consecutive Shelf objects are empty, the lower one is removed.
/// </remarks>
public class ShelfManager : MonoBehaviour {

    public Shelf shelfPrefab;
    public float verticalShelfSpacing = 4f;
    public float bottomShelfOffset = 2.2f;

    private const int INITIALSHELFCOUNT = 3;

    private List<Shelf> myShelves;
    private GameObject shelfContainer;
    private int lastShelfUsed;

	void Awake ()
    {
        if (shelfPrefab == null)
        {
            Debug.LogError("ShelfManager missing a link to the IngredientSpawner prefab.");
        }

        // Setup the structures that store shelf data and in game graphics.
        myShelves = new List<Shelf>(INITIALSHELFCOUNT);
        shelfContainer = new GameObject("ShelfContainer");
        shelfContainer.transform.parent = this.transform;
        lastShelfUsed = 0;

        // Initialize the shelves, store a reference 
        // to each and place them in the scene.
        for (int i = 0; i < INITIALSHELFCOUNT; i++)
        {
            AddExtraShelf();
        }

        // Begin play with the first two shelves showing.
        JumpToShelf(1);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Place the given IngredientSpawner on a shelf after 
    /// all other previously added IngredientSPawners.
    /// </summary>
    /// <remarks>
    /// If there is no room to place it at the end of the last shelf, a new 
    /// shelf should be added and the ingredient placed at it's beginning.
    /// </remarks>
    /// <param name="toAdd">The spawner that is to be placed on a shelf.</param>
    /// <returns>
    /// </returns>
    public int AddSpawner(IngredientSpawner toAdd)
    {
        // 1-D bin-packing problem
        // TODO: properly add each ingredient to a shelf.
        toAdd.transform.position = new Vector3(-10f, 4.5f + IngredientManager.CLIPPINGOFFSET, -5f);
        toAdd.transform.rotation = Quaternion.Euler(0f, 270f, 0f);
        return lastShelfUsed;
    }

    /// <summary>
    /// Instantiate a new Shelf and set it up to be managed by this ShelfManager.
    /// </summary>
    public void AddExtraShelf()
    {
        Shelf shelf;
        float yOffset;

        // Instantiate the shelf and attach it to the self container.
        shelf = Instantiate<Shelf>(shelfPrefab, shelfContainer.transform);

        // Move the shelf to the correct position inside the container.
        yOffset = -myShelves.Count * verticalShelfSpacing;
        shelf.transform.localPosition = new Vector3(0f, yOffset);

        // Store a reference to the shelf.
        myShelves.Add(shelf);
    }

    /// <summary>
    /// Make a given numbered shelf appear in the Scene as the 
    /// first shelf from the bottom of the Shelving Unit prefab. 
    /// </summary>
    /// <remarks>
    /// Higher numbered shelves apear below and are obscured by the Shelving Unit geometry.
    /// A few lower numbered shelves might be visible above, 
    /// depending on the Shelving Unit's geometry.
    /// If the shelf number is 0, indicating the first shelf, then only it will be visible.
    /// </remarks>
    /// <param name="shelfNumber">
    /// The number of the shelf that is to be shown at the bottom.
    /// </param>
    public void JumpToShelf(int shelfNumber)
    {
        // Clamp to existing shelf numbers.
        if (shelfNumber < 0)
        {
            shelfNumber = 0;
        }
        else if (shelfNumber >= myShelves.Count)
        {
            shelfNumber = myShelves.Count - 1;
        }

        // The container needs to move up to show higher numbered shelves.
        // Remember that the higher the shelf number, 
        // the further down they are placed in local space. 
        float yOffset = shelfNumber * verticalShelfSpacing + bottomShelfOffset;
        shelfContainer.transform.localPosition = new Vector3(0f, yOffset);
    }

}
