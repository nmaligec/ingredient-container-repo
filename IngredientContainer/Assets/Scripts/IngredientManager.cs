﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handle instantiating <see cref="IngredientSpawner"/> objects and placing them in the scene.
/// Create one spawner for each <see cref="Ingredient"/> that is unlocked.
/// </summary>
public class IngredientManager : MonoBehaviour
{
    public const float CLIPPINGOFFSET = 0.001f;

    public ShelfManager shelfManager;
    public IngredientSpawner spawnerPrefab;
    public IngredientData[] allIngredientData;

    private Dictionary<int, IngredientSpawner> spawnersByID;
    private Dictionary<string, IngredientSpawner> spawnersByName;

    void Awake()
    {
        IngredientSpawner spawner;

        if (shelfManager == null)
        {
            Debug.LogError("IngredientManager missing a link to the ShelfManager instance.");
        }
        if (spawnerPrefab == null)
        {
            Debug.LogError("IngredientManager missing a link to the IngredientSpawner prefab.");
        }

        if (allIngredientData.Length < 1)
        {
            Debug.LogError("IngredientManager has no IngredientData entries. Fill in at least one.");
        }

        // Instantiate a spawner for each unlocked Ingredient and store the reference.
        spawnersByID = new Dictionary<int, IngredientSpawner>();
        spawnersByName = new Dictionary<string, IngredientSpawner>();
        foreach (IngredientData entry in allIngredientData)
        {
            if (entry.isUnlocked)
            {
                spawner = CreateSpawner(entry, true);
                spawnersByID.Add(entry.id, spawner);
                spawnersByName.Add(entry.shortName, spawner);
                AddToShelf(spawner);
            }
        }
    }

    /// <summary>
    /// Creates an IngredientSpawner for a given IngredientData type, at given 
    /// position and rotation, with the spawner's graphics optionally visible.
    /// </summary>
    /// <param name="entry">
    /// The IngredientData entry that contains initialisation parameters 
    /// for the IngredientSpawner and it's Ingredient.
    /// </param>
    /// <param name = "isVisible" >
    /// Whether or not to show graphics for the spawner and 
    /// let it interact with other objects in the Scene.
    /// <returns>
    /// The initialized spawner for a particular Ingridient type, 
    /// as given by the supplied IngridientData.
    /// </returns>
    public IngredientSpawner CreateSpawner(IngredientData entry, bool isVisible = false)
    {
        return CreateSpawner(entry, Vector3.zero, Quaternion.identity, isVisible);
    }

    /// <summary>
    /// Creates an IngredientSpawner for a given IngredientData type, at given 
    /// position and rotation, with the spawner's graphics optionally visible.
    /// </summary>
    /// <param name="entry">
    /// The IngredientData entry that contains initialisation parameters 
    /// for the IngredientSpawner and it's Ingredient.
    /// </param>
    /// <param name="pos">
    /// The position, in World coordinates, to place the IngredientSpawner.
    /// </param>
    /// <param name = "isVisible" >
    /// Whether or not to show graphics for the spawner and 
    /// let it interact with other objects in the Scene.
    /// </param>
    /// <returns>
    /// The initialized spawner for a particular Ingridient type, 
    /// as given by the supplied IngridientData.
    /// </returns>
    public IngredientSpawner CreateSpawner(IngredientData entry, Vector3 pos, 
                              bool isVisible = false)
    {
        return CreateSpawner(entry, pos, Quaternion.identity, isVisible);
    }

    /// <summary>
    /// Creates an IngredientSpawner for a given IngredientData type, at given 
    /// position and rotation, with the spawner's graphics optionally visible.
    /// </summary>
    /// <param name="entry">
    /// The IngredientData entry that contains initialisation parameters 
    /// for the IngredientSpawner and it's Ingredient.
    /// </param>
    /// <param name="pos">
    /// The position, in World coordinates, to place the IngredientSpawner.
    /// </param>
    /// <param name="rot">
    /// The rotation of the IngredientSpawner.
    /// </param>
    /// <param name = "isVisible" >
    /// Whether or not to show graphics for the spawner and 
    /// let it interact with other objects in the Scene.
    /// </param>
    /// <returns>
    /// The initialized spawner for a particular Ingridient type, 
    /// as given by the supplied IngridientData.
    /// </returns>
    public IngredientSpawner CreateSpawner(IngredientData entry, Vector3 pos, Quaternion rot, 
                              bool isVisible = false)
    {
        IngredientSpawner spawner = 
            Instantiate<IngredientSpawner>(spawnerPrefab, pos, rot, this.transform);

        // Initialize the spawner with the unlocked Ingredient.
        spawner.Init(entry, isVisible);

        return spawner;
    }

    /// <summary>
    /// Gets the IngredientSpawner with the given id.
    /// </summary>
    /// <param name="id">The id of the IngredientSpawner to retrieve.</param>
    /// <returns>
    /// The IngredientSpawner for a particular Ingridient type, 
    /// as designated by the given id.
    /// Returns null if there is no spawner with that id.
    /// </returns>
    public IngredientSpawner GetSpawner(int id)
    {
        if (spawnersByID.ContainsKey(id))
        {
            return spawnersByID[id];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Gets the IngredientSpawner with the given name.
    /// </summary>
    /// <param name="name">The name of the IngredientSpawner to retrieve.</param>
    /// <returns>
    /// The IngredientSpawner for a particular Ingridient type, 
    /// as designated by the given name.
    /// Returns null if there is no spawner with that name.
    /// </returns>
    public IngredientSpawner GetSpawner(string name)
    {
        if (spawnersByName.ContainsKey(name))
        {
            return spawnersByName[name];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Places a given IngredientSpawner on a Shelf and optionally 
    /// displays to the player a visual cue of where it ws added.
    /// </summary>
    /// <remarks>
    /// The IngredientSpawner will be added at the end, after all previously added ones.
    /// Note: a more efficient packing method could be used, but always placing 
    /// on the end is more intuitive for a players.
    /// </remarks>
    /// <param name="spawner">
    /// The IngredientSpawner to be placed on a Shelf.
    /// </param>
    /// <param name="shouldHighlight">
    /// If set to <c>true</c> displays the shelf it was placed on and highlights it.
    /// </param>
    public void AddToShelf(IngredientSpawner spawner, bool shouldHighlight = false)
    {
        int shelfIndex = shelfManager.AddSpawner(spawner);

        if (shouldHighlight)
        {
            // TODO: add object highlighting to the spawner to show the player where it was added.
            shelfManager.JumpToShelf(shelfIndex);
        }
    }

    /// <summary>
    /// Shows the spawner's pedestal graphics and has the Ingredient spawn ontop.
    /// </summary>
    /// <param name="spawner">The spawner to show.</param>
    public void ShowSpawner(IngredientSpawner spawner)
    {
        spawner.EnablePedestal();
    }

    /// <summary>
    /// Hides the spawner's pedestal graphics and has the Ingredient spawn at it's center.
    /// </summary>
    /// <param name="spawner">The spawner to hide.</param>
    public void HideSpawner(IngredientSpawner spawner)
    {
        spawner.DisablePedestal();
    }

    /// <summary>
    /// Moves the spawner.
    /// </summary>
    /// <param name="spawner">The spawner to move.</param>
    /// <param name="pos">The new position for the spawner.</param>
    public void MoveSpawner(IngredientSpawner spawner, Vector3 pos)
    {
        spawner.transform.position = pos;
    }

    /// <summary>
    /// Rotates the spawner.
    /// </summary>
    /// <param name="spawner">The spawner to rotate.</param>
    /// <param name="rot">The new rotation for the spawner.</param>
    public void RotateSpawner(IngredientSpawner spawner, Quaternion rot)
    {
        spawner.transform.rotation = rot;
    }
}
