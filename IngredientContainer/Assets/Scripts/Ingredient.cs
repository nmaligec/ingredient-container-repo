﻿//using System;
using UnityEngine;

/// <summary>
/// Manages interactions and data for a unique ingredient.
/// </summary>
[RequireComponent(typeof(Collider), typeof(Rigidbody))]
public class Ingredient : MonoBehaviour, IUseable
{
    // Variables for managing ingredient state.
    private IUser interactingObject;
    private bool isDespawning;

    // Store component references.
    private Collider myCollider;
    private Rigidbody myRigidbody;

    public IngredientData Data { get; private set; }
    public Bounds MyBounds
    {
        get
        {
            if (myCollider != null)
            {
                return myCollider.bounds;
            }
            else
            {
                return new Bounds();
            }
        }
    }

    void OnEnable()
    {
        // Set variables that should be reset between spawns.
        interactingObject = null;
        isDespawning = false;
    }

    void OnDrawGizmos()
    {
        if (myCollider != null)
        {
            Gizmos.DrawWireCube(myCollider.bounds.center, myCollider.bounds.size);
        }
    }

    /// <summary>
    /// Initialize the Ingredient using the given IngredientData.
    /// </summary>
    /// <remarks>
    /// Any information shared by all Ingredients of the same kind 
    /// is stored in the given IngredientData entry. This includes 
    /// a reference to the IngredientSpawner that spawns and manages 
    /// the object pool this Ingredient belongs to.
    /// </remarks>
    /// <param name="data">
    /// The data entry that contains initialisation parameters to use for this instance.
    /// </param>
    public void Init(IngredientData data)
    {
        Data = data;

        // Set the correct layer for ingredients.
        this.gameObject.layer = LayerMask.NameToLayer("Ingredients");

        // Set component references.
        myCollider = GetComponent<Collider>();
        myRigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Despawns the Ingredient after a given amount of time has passed.
    /// </summary>
    /// <param name="delay">
    /// The amount of time, in seconds, to wait before despawning.
    /// </param>
    public void DespawnStart(float delay)
    {
        if (!isDespawning)
        {
            Invoke("DespawnNow", delay);
            isDespawning = true;
        }
    }

    /// <summary>
    /// Cancels the delayed despawn initiated though DespawnStart.
    /// </summary>
    public void DespawnCancel()
    {
        if (isDespawning)
        {
            CancelInvoke("DespawnNow");
            isDespawning = false;
        }
    }

    /// <summary>
    /// Immediately despawns the Ingredient by hiding it and disabling all it 
    /// functionality.
    /// </summary>
    public void DespawnNow()
    {
        // Inform objects that were using this Ingredient to cancel their interaction.
        if (interactingObject != null)
        {
            interactingObject.CancelUsing();
        }

        // Initiate respawn if an IngredientSpawner has
        // been assigned to this Ingredient.
        if ((Data != null) && (Data.MySpawner != null))
        {
            Data.MySpawner.ReclaimPooledIngredient(this);
        }
        else
        {
            // Nowhere else to go, so clear it from memory.
            Destroy(this.gameObject);
        }
    }

    /// <inheritdoc cref="IUseable.StartUse(IUser)" />
    /// <remarks>
    /// The user object will also control this object's movement.
    /// </remarks>
    public void StartUse(IUser user)
    {
        interactingObject = user;
        interactingObject.ControlMovement();
        myCollider.enabled = false;
        myRigidbody.detectCollisions = false;
        myRigidbody.isKinematic = true;
        DespawnCancel();
    }

    /// <inheritdoc cref="IUseable.StopUse" />
    /// <remarks>
    /// When interaction ends, the user object will restore control of this 
    /// object's movement.
    /// </remarks>
    public void StopUse()
    {
        interactingObject.ReleaseMovement();
        interactingObject = null;
        myCollider.enabled = true;
        myRigidbody.detectCollisions = true;
        myRigidbody.isKinematic = false;
    }
}
