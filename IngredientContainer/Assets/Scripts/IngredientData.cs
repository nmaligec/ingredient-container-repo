﻿using System;
using UnityEngine;

/// <summary>
/// Stores all information for a specific <see cref="Ingredient"/>.
/// Includes initialisation parameters for the associated <see cref="IngredientSpawner"/>,
/// as well as properties common to all Ingredients of the same kind. 
/// </summary>
[Serializable]
public class IngredientData
{
    public int id;
    public string shortName;
    public string fullName;
    public bool isUnlocked;
    public GameObject ingredientModel;
    public Color baseColor;

    // Spawner settings.
    public float respawnTime;
    public int spawnerPoolSize;
    public float spawnerMargins;
    public float spawnerHeight;

    public IngredientSpawner MySpawner { get; set; }

    public IngredientData()
    {
        id = 0;
        shortName = "";
        fullName = "";
        isUnlocked = false;
        ingredientModel = null;
        baseColor = Color.white;
        respawnTime = 5f;
        spawnerPoolSize = 3;
        spawnerMargins = 0.2f;
        spawnerHeight = 0.1f;
    }
}
