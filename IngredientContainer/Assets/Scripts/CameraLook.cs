﻿using UnityEngine;

/// <summary>
/// Simple Camera control script, which just enables looking around without moving.
/// </summary>
/// <remarks>
/// Includes ability to limit the maximum looking up and down angles.
/// </remarks>
[RequireComponent(typeof(Camera))]
public class CameraLook : MonoBehaviour {

    public float verticalMax = -35.0f;    // Looking up starts at 0 and goes to -90.
    public float verticalMin = 20.0f;     // Looking down starts at 0 and goes to 90.
    public float horizontalSpeed = 2.0f;
    public float verticalSpeed = 1.5f;

    private float yRot = 0;    // Left/Right movement.
    private float xRot = 0;    // Up/Down movement.

    void Update () {

        // Rotate Camera based on inputs.
        if (Input.GetButton("Fire2"))
        {
            yRot += Input.GetAxis("Mouse X") * Time.deltaTime * horizontalSpeed;
            xRot -= Input.GetAxis("Mouse Y") * Time.deltaTime * verticalSpeed;
            xRot = Mathf.Clamp(xRot, verticalMax, verticalMin);
            transform.localEulerAngles = new Vector3 (xRot, yRot, 0f);
        }
    }
}
