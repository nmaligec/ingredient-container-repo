﻿using System;
using UnityEngine;

/// <summary>
/// Manage the player's hand and its actions in the game world.
/// </summary>
[RequireComponent(typeof(Animator))]
public class PlayerHandController : MonoBehaviour, IUser
{
    public GameObject handGraphics;
    public float interactionRadius = 1f;

    // Arm (hand) stretch limits.
    public float maxStretchDistance = 10.0f;
    public float minStretchDistance = 2.0f;
    public float stretchSpeed = 5.0f;

    // Stores hand offsets from view center.
    // Set based on initial position of this object in the editor.
    private float horizontalOffset;
    private float verticalOffset;
    private float stretchDistance;

    // Variables for managing hand state.
    private Action processHoldingState;
    private GameObject interactingObject;

    // Color presets to indicate different hand states.
    private Color normalColor;
    private Color hoverColor;

    // Store component references.
    private Material handMaterial;
    private Animator myAnimator;
    private int isHandClosed_AnimatorID;

    // Store layermask ids.
    private LayerMask ingredientMask;

    // Use this for initialization
    void Start()
    {
        MeshRenderer handRenderer;

        // Set component references.
        if (handGraphics != null)
        {
            handRenderer = handGraphics.GetComponent<MeshRenderer>();
            if (handRenderer != null)
            {
                handMaterial = handRenderer.material;
                normalColor = handMaterial.color;
                hoverColor = Color.blue;
                hoverColor.a = normalColor.a;
            }
            else
            {
                Debug.LogError("Missing graphics for PlayerHand: MeshRenderer.");
            }
        }
        else
        {
            Debug.LogError("Missing link to PlayerHandGraphics.");
        }
        myAnimator = GetComponent<Animator>();  // Guaranteed to exist.
        isHandClosed_AnimatorID = Animator.StringToHash("IsHandClosed");

        // Get hand offsets from initial placement in the scene.
        horizontalOffset = transform.localPosition.x;
        verticalOffset = transform.localPosition.y;
        stretchDistance = transform.localPosition.z;

        // Set initial state.
        processHoldingState = CheckGrab;
        interactingObject = null;

        // Cache layermask values.
        ingredientMask = LayerMask.GetMask("Ingredients");
    }

    // Update is called once per frame
    void Update()
    {
        ProcessHandDistance();
        processHoldingState();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, interactionRadius);
    }

    /// <inheritdoc cref="IUser.ControlMovement()" />
    public void ControlMovement()
    {
        interactingObject.transform.position = this.transform.position + this.transform.forward * 1.1f;
        interactingObject.transform.parent = this.transform;
    }


    /// <inheritdoc cref="IUser.ReleaseMovement()" />
    public void ReleaseMovement()
    {
        interactingObject.transform.parent = null;
    }

    /// <inheritdoc cref="IUser.CancelUsing()" />
    public void CancelUsing()
    {
        processHoldingState = CheckGrab;
        myAnimator.SetBool(isHandClosed_AnimatorID, false);
        handMaterial.color = normalColor;
        interactingObject = null;
    }

    /// <summary>
    /// Moves the entire hand object, including graphics, 
    /// closer or further from the main camera.
    /// </summary>
    /// <remarks>
    /// Distance moved is based on player input.
    /// </remarks>
    private void ProcessHandDistance()
    {
        stretchDistance += Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * stretchSpeed;
        stretchDistance = Mathf.Clamp(stretchDistance, minStretchDistance, maxStretchDistance);
        transform.localPosition =
                     new Vector3(horizontalOffset, verticalOffset, stretchDistance);
    }

    /// <summary>
    /// Changes the hand state when the player begins interacting with a useable object.
    /// </summary>
    /// <remarks>
    /// The player's hand must be in range of a valid useable object to interact with it.
    /// The interaction is initiated by user input.
    /// </remarks>
    private void CheckGrab()
    {
        IUseable useable;
        Collider[] hitColliders;
        GameObject touching = null;

        // Check hover state.
        // Find any Ingredient objects that are in interaction range.
        hitColliders = Physics.OverlapSphere(this.transform.position,
                                             interactionRadius,
                                             ingredientMask,
                                             QueryTriggerInteraction.Ignore);

        // If Ingredient objects are in range, select 
        // one and set the hand state to hovering.
        if (hitColliders.Length > 0)
        {
            handMaterial.color = hoverColor;
            touching = hitColliders[0].gameObject;
        }
        else
        {
            handMaterial.color = normalColor;
        }

        // Check grab state.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Set state that doesn't rely on having an interaction.
            processHoldingState = CheckDrop;
            myAnimator.SetBool(isHandClosed_AnimatorID, true);

            // Check for interaction with an object.
            if ((touching != null) && (interactingObject == null))
            {
                useable = touching.GetComponent<IUseable>();
                if (useable != null)
                {
                    // Set interacting state.
                    handMaterial.color = normalColor;
                    interactingObject = touching; // Must be set before calling StartUse().

                    // Set state for object being interacted with.
                    useable.StartUse(this);
                }
            }
        }
    }

    /// <summary>
    /// Changes the hand state when the player stops interacting with a useable object.
    /// </summary>
    /// <remarks>
    /// The player must already be interacting with a useable object.
    /// The end of interaction is initiated by user input.
    /// </remarks>
    private void CheckDrop()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            // Set state that doesn't rely on having an interaction.
            processHoldingState = CheckGrab;
            myAnimator.SetBool(isHandClosed_AnimatorID, false);

            // Check if it is also an end of interaction.
            if (interactingObject != null)
            {
                // Clear interaction state for the object previously interacted with.
                interactingObject.GetComponent<IUseable>().StopUse();

                // Clear interaction state.
                interactingObject = null;
            }
        }
    }
}
