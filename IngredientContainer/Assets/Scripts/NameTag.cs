﻿using TMPro;
using UnityEngine;

/// <summary>
/// Handles displaying name text infront of an object.
/// </summary>
/// <remarks>
/// The front of an object is determined by the minimum point on it's local z-axis.
/// </remarks>
public class NameTag : MonoBehaviour
{
    public TextMeshPro nameText;
    public GameObject nameplateGraphics;

    // Keep the thickness of the back plate constant.
    private float plateLength;

    void Awake()
    {
        if (nameText == null)
        {
            Debug.LogError("NameTag is missing a link to TextMeshPro");
        }
        if ((nameplateGraphics == null)
            || (nameplateGraphics.GetComponent<MeshFilter>() == null)
            || (nameplateGraphics.GetComponent<MeshRenderer>() == null))
        {
            Debug.LogError("NameTag is missing a link to it's graphics.");
        }
        else
        {
            // Set the thickness only once, based on the thickness of the prefab.
            plateLength = nameplateGraphics.GetComponent<MeshRenderer>().bounds.extents.z;
        }
    }

    /// <summary>
    /// Initialize the NameTag with a given name and placed 
    /// in front of a GameObject given by the extents.
    /// </summary>
    /// <param name="ownerExtents">
    /// The half measurements of the object to fit the NameTag in front of.
    /// </param>
    /// <param name="displayName">
    /// The name to be shown on the NameTag.
    /// </param>
    public void Init(Vector3 ownerExtents, string displayName)
    {
        PlaceInFront(ownerExtents.y, ownerExtents.z);
        SetDisplaySize(ownerExtents.x, ownerExtents.y * 2f);
        SetDisplayText(displayName);
        AutoSizeFont();
    }

    /// <summary>
    /// Places the NameTag in front of the GameObject that it displays a name for.
    /// </summary>
    /// <remarks>
    /// The NameTag is ideally flush with the bottom and front of the object, 
    /// as given by the y and z offsets respectively.
    /// It is forced to be centered along the x-axis.
    /// </remarks>
    /// <param name="yOffset">
    /// The amount to shift the NameTag up, so that it's flush with the bottom.
    /// </param>
    /// <param name="zOffset">
    /// The amount to shift the NameTag back, so that it's flush with the front.
    /// </param>
    public void PlaceInFront(float yOffset, float zOffset)
    {
        // Move the NameTag group into place.
        Vector3 v = this.transform.localPosition;
        v.y += yOffset;
        v.z -= zOffset + IngredientManager.CLIPPINGOFFSET;
        this.transform.localPosition = v;

        // Make sure the name plate portion isn't clipping the front.
        v = nameplateGraphics.transform.localPosition;
        v.z = -plateLength;
        nameplateGraphics.transform.localPosition = v;

        // Make sure the name text isn't behind the name plate or clipping it.
        v.z -= plateLength + IngredientManager.CLIPPINGOFFSET;
        nameText.transform.localPosition = v;
    }

    /// <summary>
    /// Sets the size of the display area.
    /// </summary>
    /// <remarks>
    /// The display area is forced to be along the x and y axes.
    /// After changing the display area, it's recommended to call AutoSizeFont. 
    /// </remarks>
    /// <param name="xExtent">The new half-width of the display area.</param>
    /// <param name="yExtent">The new half-height of the display area.</param>
    public void SetDisplaySize(float xExtent, float yExtent)
    {
        RectTransform r = nameText.GetComponent<RectTransform>();

        // Resize the text area.
        r.sizeDelta = new Vector2(xExtent, yExtent) * 2f;

        // Resize the name plate/backing graphics behind the text.
        nameplateGraphics.transform.localScale = new Vector3(xExtent, yExtent, plateLength);
    }

    /// <summary>
    /// Sets the text to display on the NameTag.
    /// </summary>
    /// <remarks>
    /// After changing the text, it's recommended to call AutoSizeFont.
    /// </remarks>
    /// <param name="displayText">
    /// The text to display on the NameTag.
    /// </param>
    public void SetDisplayText(string displayText)
    {
        nameText.text = displayText;
    }

    /// <summary>
    /// Makes the TextMeshPro component do a best fit for the display text.
    /// </summary>
    /// <remarks>
    /// This should be called after SetDisplaySize and SetDisplayText 
    /// are finished making changes.
    /// </remarks>
    public void AutoSizeFont()
    {
        // For performance reasons, TMP docs recomend enabling 
        // the auto size feature for only as long as needed.
        nameText.enableAutoSizing = true;
        nameText.ForceMeshUpdate();
        nameText.enableAutoSizing = false;
    }
}
