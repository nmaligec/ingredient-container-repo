﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages multiple <see cref="IngredientSpawner"/> objects which in turn
/// manage<see cref="Ingredient"/>s.
/// </summary>
/// <remarks>
/// When A new Ingredient is placed on a Shelf, it creates a corresponding 
/// IngredientSpawner.
/// </remarks>
public class Shelf : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
