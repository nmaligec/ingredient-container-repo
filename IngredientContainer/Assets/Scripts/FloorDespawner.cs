﻿using UnityEngine;

/// <summary>
/// Recycle objects that sit uselessly on the floor for too long.
/// </summary>
public class FloorDespawner : MonoBehaviour
{

    public float despawnTime = 60.0f;

    void OnTriggerEnter(Collider other)
    {
        Ingredient i = other.GetComponent<Ingredient>();
        if (i != null)
        {
            i.DespawnStart(despawnTime);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Ingredient i = other.GetComponent<Ingredient>();
        if (i != null)
        {
            i.DespawnCancel();
        }

    }
}
