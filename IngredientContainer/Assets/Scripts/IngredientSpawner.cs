﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the display of a specific <see cref="Ingredient"/> on a <see cref="Shelf"/>.
/// </summary>
/// <remarks>
/// When the Ingredient is used or destroyed, a new one is created at the spawner's location.
/// An IngredientSpawner can be moved around a Shelf or even to a different Shelf.
/// </remarks>
[RequireComponent(typeof(BoxCollider))]
public class IngredientSpawner : MonoBehaviour
{
    public GameObject spawnerGraphics;
    public NameTag nameTag;

    private IngredientData myIngredientData;
    private float ingredientHeight;
    private Bounds spawnArea;

    // State variables.
    private bool isRespawning;

    // Object pool variables.
    private List<Ingredient> ingredientPool;
    private Ingredient activeIngredient;

    // Cached Component references.
    private BoxCollider myCollider;

    public bool IsVisible
    {
        get
        {
            return (spawnerGraphics != null) && (spawnerGraphics.activeInHierarchy);
        }
    }

    void Awake()
    {
        if ((spawnerGraphics == null)
            || (spawnerGraphics.GetComponent<MeshFilter>() == null)
            || (spawnerGraphics.GetComponent<MeshRenderer>() == null))
        {
            Debug.LogError("IngredientSpawner prefab is missing a link to it's graphics.");
        }
        if(nameTag == null)
        {
            Debug.LogError("IngredientSpawner prefab is missing a link to it's NameTag.");
        }
    }

    void FixedUpdate()
    {
        // Check if the active ingredient is still near the spawner.
        if (activeIngredient != null)
        {
            // Reset the spawnArea, in case this spawner was moved.
            spawnArea.center = GetSpawnPosition();

            // Respawning a new ingredient if the active
            // ingredient is not in the spawn area.
            if (!spawnArea.Intersects(activeIngredient.MyBounds))
            {
                activeIngredient = null;
                RespawnStart(myIngredientData.respawnTime);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(spawnArea.center, spawnArea.size);
    }

    /// <summary>
    /// Initialize the spawner to a specific instance of an ingredient.
    /// </summary>
    /// <remarks>
    /// If the spawner is to be visible, it will need to match 
    /// the size of the ingredient and always be visually under 
    /// the ingredient when one is spawned.
    /// </remarks>
    /// <param name="spawnData">
    /// All data concerning the specific type of Ingredient this spawner creates.
    /// </param>
    /// <param name="isVisible">
    /// Whether or not to show graphics for the spawner and 
    /// let it interact with other objects in the Scene.
    /// </param>
    public void Init(IngredientData spawnData, bool isVisible = false)
    {
        myIngredientData = spawnData;
        this.gameObject.name = spawnData.shortName + "Spawner";

        // Set component references.
        myCollider = GetComponent<BoxCollider>();

        // Setup graphics and spawn offsets.
        spawnArea = new Bounds();
        if (isVisible)
        {
            EnablePedestal();
        }
        else
        {
            DisablePedestal();
        }

        // Store the reference to this spawner in the IngredientData entry,
        // so all Ingredients that this spawner manages can access it easily.
        spawnData.MySpawner = this;

        // Initialise the object pool.
        ingredientPool = new List<Ingredient>(spawnData.spawnerPoolSize);

        // Add initial objects to the pool
        for (int i = 0; i < spawnData.spawnerPoolSize; i++)
        {
            CreatePooledIngredient();
        }

        // Spawn in the very first ingredient.
        RespawnNow();
    }

    /// <summary>
    /// Respawns the spawner's Ingredient after a delay.
    /// </summary>
    /// <param name="delay">
    /// The time in seconds before the Ingredient respawns.
    /// </param>
    public void RespawnStart(float delay)
    {
        if (!isRespawning)
        {
            Invoke("RespawnNow", delay);
            isRespawning = true;
        }
    }

    /// <summary>
    /// Stop the respawing of an Ingredient if it was set to 
    /// happen after a delay and it's still counting down.
    /// </summary>
    public void RespawnCancel()
    {
        if (isRespawning)
        {
            CancelInvoke("RespawnNow");
            isRespawning = false;
        }
    }

    /// <summary>
    /// Have an associated Ingredient become visible on top of the spawner.
    /// </summary>
    public void RespawnNow()
    {
        Ingredient ing = GetPooledIngredient();

        // Move the ingredient to be spawned over the spawner.
        ing.transform.position = GetSpawnPosition();
        ing.transform.rotation = Quaternion.identity;

        // Activate the ingredient.
        ing.gameObject.SetActive(true);
        activeIngredient = ing;

        // Clear the spawning state.
        isRespawning = false;
    }

    /// <summary>
    /// Spawn an Ingredient at the given position.
    /// </summary>
    /// <remarks>
    /// This will not affect or use the currently spawned Ingredient at the spawner itself.
    /// </remarks>
    /// <param name="pos">
    /// The position in World coordinates to place the Ingredient when it's spawned.
    /// </param>
    public void SpawnAtPosition(Vector3 pos)
    {
        SpawnAtPosition(pos, Quaternion.identity);
    }

    /// <summary>
    /// Spawn an Ingredient at the given position and rotation.
    /// </summary>
    /// <remarks>
    /// This will not affect or use the currently spawned Ingredient at the spawner itself.
    /// </remarks>
    /// <param name="pos">
    /// The position in World coordinates to place the Ingredient when it's spawned.
    /// </param>
    /// <param name="rot">
    /// The optional roation to give the spawned Ingredient.
    /// </param>
    public void SpawnAtPosition(Vector3 pos, Quaternion rot)
    {
        Ingredient ing = GetPooledIngredient();

        // Move the ingredient to be spawned over the spawner.
        ing.transform.position = pos;
        ing.transform.rotation = rot;

        // Activate the ingredient.
        ing.gameObject.SetActive(true);
    }

    /// <summary>
    /// Replace a previously active Ingredient, 
    /// from this spawner, back in it's object pool.
    /// </summary>
    /// <remarks>
    /// The given Ingredient should at one point have been part of this spawner's 
    /// object pool. If not, it won't be replaced in the pool.
    /// </remarks>
    /// <param name="toReclaim">
    /// The Ingredient to add back to this spawner's object pool.
    /// </param>
    public void ReclaimPooledIngredient(Ingredient toReclaim)
    {
        // Make sure it belongs to this spawner's pool.
        // If it does, they will share the same data reference.
        if (toReclaim.Data == myIngredientData)
        {
            // Hide the Ingredient by disabling its GameObject, 
            // which also designates it as being in the pool.
            // Note: all Update code and event handlers for this script, other
            // attached Components, and those in child GameObjects will be disabled.
            toReclaim.gameObject.SetActive(false);

            // And make sure it follows the spawner's movement.
            toReclaim.transform.parent = this.transform;
        }
    }

    /// <summary>
    /// Show full spawner graphics, including: a pedestal and ingredient nametag.
    /// </summary>
    /// <remarks>
    /// The Ingredient will now spawn on top of the pedestal. The spawner will 
    /// now also be able to be interacted with, by manipulating the pedestal.
    /// </remarks>
    public void EnablePedestal()
    {
        Vector3 v;
        MeshRenderer ingredientGraphics =
            myIngredientData.ingredientModel.GetComponent<MeshRenderer>();
        if (ingredientGraphics != null)
        {

            // Set the scale of the spawner graphics to fit the ingredient. 
            v = ingredientGraphics.bounds.extents;
            v.x += myIngredientData.spawnerMargins;
            v.y = myIngredientData.spawnerHeight;
            v.z += myIngredientData.spawnerMargins;
            spawnerGraphics.transform.localScale = v;
            myCollider.size = v * 2f;

            // Make the graphics visible and reactivate the Collider.
            spawnerGraphics.SetActive(true);
            nameTag.gameObject.SetActive(true);
            myCollider.enabled = true;

            // Adjust the spawn height.
            ingredientHeight = myIngredientData.spawnerHeight
                             + ingredientGraphics.bounds.extents.y
                             + IngredientManager.CLIPPINGOFFSET;

            // Set the spawn area's new size.
            // It wont change again, unless DisablePedestal is called.
            v.y = ingredientGraphics.bounds.extents.y;
            spawnArea.extents = v;

            // Place the NameTag centered on the front with the Ingredient's name on it.
            nameTag.Init(spawnerGraphics.transform.localScale, myIngredientData.fullName);
        }
        else
        {
            Debug.LogErrorFormat("The ingredient {0} is missing a link to it's graphics.",
                                 myIngredientData.shortName);
        }
    }

    /// <summary>
    /// Hide all associated spawner graphics, except the Ingredient that it spawns.
    /// </summary>
    /// <remarks>
    /// The Ingredient will spawn at the center of the invisible spawner.
    /// The spawner will also no longer be interactable.
    /// </remarks>
    public void DisablePedestal()
    {
        // The disable the graphics to hide them.
        spawnerGraphics.SetActive(false);
        nameTag.gameObject.SetActive(false);

        // Disable the collider and thus interaction, including placyer interaction.
        myCollider.enabled = false;

        // Set the Ingredient to spawn at the spawner's exact position.
        ingredientHeight = 0f;

        // Set the spawn area's new size.
        // It wont change again, unless EnablePedestal is called.
        spawnArea.extents = myCollider.bounds.extents;
    }


    /// <summary>
    /// Get the position, in World coordinates, that 
    /// the ingredient should appear at when spawned.
    /// </summary>
    /// <returns>
    /// The adjusted position for the Ingredient to fit properly on the spawner. 
    /// </returns>
    private Vector3 GetSpawnPosition()
    {
        Vector3 pos = transform.position;
        pos.y += ingredientHeight;
        return pos;
    }

    /// <summary>
    /// Get the next available Ingredient from the spawner's object pool.
    /// </summary>
    /// <remarks>
    /// If there are no more Ingredients in the pool, 
    /// create a new one and add to the pool.
    /// </remarks>
    /// <returns>
    /// An instantiated but inactive Ingredient from the spawner's object pool.
    /// </returns>
    private Ingredient GetPooledIngredient()
    {
        for (int i = 0; i < ingredientPool.Count; i++)
        {
            if (!ingredientPool[i].gameObject.activeInHierarchy)
            {
                return ingredientPool[i];
            }
        }
        CreatePooledIngredient();
        return ingredientPool[ingredientPool.Count - 1];
    }

    /// <summary>
    /// Instantiates a GameObject to represent an Ingredient 
    /// and adds it to this spawner's object pool.
    /// </summary>
    /// <remarks>
    /// An instantiated Ingredient is based from the associated art asset.
    /// The Ingredient Component is added, as well as the required 
    /// Collider and Rigidbody for physics engine and player interactions.
    /// </remarks>
    private void CreatePooledIngredient()
    {
        GameObject go;
        Ingredient ing;
        MeshCollider mc;

        // Instantiate an Ingredient.
        go = Instantiate(myIngredientData.ingredientModel, this.transform);
        mc = go.AddComponent<MeshCollider>();
        mc.convex = true;
        go.AddComponent<Rigidbody>();
        ing = go.AddComponent<Ingredient>() as Ingredient;

        // Initialise the Ingredient.
        ing.Init(myIngredientData);
        go.tag = "Ingredient";
        go.name = myIngredientData.shortName + " " + ingredientPool.Count;

        // Disable it until needed, to save on processing and to hide it's graphics.
        // Also, being inactive designates it as being available for the ingredient pool.
        go.SetActive(false);

        // Add it to the pool.
        ingredientPool.Add(ing);
    }
}
